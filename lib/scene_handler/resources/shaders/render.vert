#version 430 core

#undef lowp
#undef mediump
#undef highp
precision highp float;

struct Camera
{
    mat4 projection;
	mat3 rotate;
    vec3 at;
    vec3 position;
	vec3 scale;
};
vec3 fromCameraTo(Camera camera, vec3 p)
{
    return camera.scale * (p - camera.at) - camera.position;
}
layout(binding = 0, std140) uniform Block
{
    Camera camera;
};

struct Prop
{
    vec4 pos;
    vec4 velocity;
};

layout(std430, binding = 0) buffer Props
{
    Prop props[];
};

out vec4 propColor;

void main()
{
	float sqSize = 0.002f;
	Prop prop = props[gl_InstanceID];
	
    propColor = vec4(1.f, 0.f, 0.f, 1.f);
	
	vec3 pos = camera.rotate * fromCameraTo(camera, prop.pos.xyz);
	if(0 == gl_VertexID) {
        pos += vec3(-sqSize, sqSize, 0.f);
    }
	else if(1 == gl_VertexID) {
        pos += vec3(sqSize, sqSize, 0.f);
    }
	else if(2 == gl_VertexID) {
        pos += vec3(-sqSize, -sqSize, 0.f);
    }
	else if(3 == gl_VertexID) {
        pos += vec3(sqSize, -sqSize, 0.f);
    }
	
	gl_Position = camera.projection * vec4(pos, 1.f);
}