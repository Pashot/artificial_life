#version 430 core

#undef lowp
#undef mediump
#undef highp
precision highp float;

in vec4 propColor;

layout (location = 0) out vec4 frag;

void main()
{
	frag = propColor;
}
