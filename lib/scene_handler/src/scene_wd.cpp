#include <scene_wd.h>
#include <abstract_tool_panel.h>
#include <camera.h>

#include <QMouseEvent>
#include <QOpenGLExtraFunctions>
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QDebug>
#include <QTimer>

#include <optional>

void InitResources()
{
    Q_INIT_RESOURCE(shaders);
    Q_INIT_RESOURCE(test_data);
}

namespace scene_handler
{

constexpr int GROUP_NUM = 16384 / 32;
constexpr int THREADS_IN_GROUP = 1024;
constexpr int TOTAL_THREADS_NUM = GROUP_NUM * THREADS_IN_GROUP;

struct Prop
{
    alignas(16) QVector3D pos;
    alignas(16) QVector3D velocity;
};
void serialize(Prop const& prop, QTextStream& stream)
{
    stream << prop.pos.x() << " " << prop.pos.y() << " " << prop.pos.z() << " "
        << prop.velocity.x() << " " << prop.velocity.y() << " " << prop.velocity.z() << "\n";
}

struct OpenGLWidget::PrivateData
{
    std::vector<tool_panels::AbstractToolPanel*> panels;

    std::vector<Prop> props;
    int propsMaxNum;

    Camera camera;
    std::optional<QPoint> lastMousePoint{ std::nullopt };
    std::unique_ptr<QOpenGLShaderProgram> renderProgram;
    std::unique_ptr<QOpenGLShaderProgram> computeProgram;
    int computeSpeed{ 1 };

    GLuint renderVAO = 0;
    GLuint renderEBO = 0;
    GLuint renderUBO = 0;

    GLuint propsSSBO = 0;
};

OpenGLWidget::OpenGLWidget(QWidget* parent)
    : QOpenGLWidget(parent)
    , m_pData(std::make_unique<PrivateData>())
{
    setMouseTracking(true);
    InitResources();
    loadCostMap();

    QTimer::singleShot(0, this, &OpenGLWidget::loadParameters);
}

OpenGLWidget::~OpenGLWidget()
{

}

QSize OpenGLWidget::sizeHint() const
{
    return QSize(1000, 1000);
}

void OpenGLWidget::resizeEvent(QResizeEvent* pEvent)
{
    QOpenGLWidget::resizeEvent(pEvent);

    for (auto pPanel : m_pData->panels)
    {
        if (nullptr != pPanel)
        {
            pPanel->moveSelf();
        }
    }
}

void OpenGLWidget::addPanel(tool_panels::AbstractToolPanel* pPanel)
{
    m_pData->panels.push_back(pPanel);
}

void OpenGLWidget::onSaveButtonClicked()
{
    auto fileName = QFileDialog::getSaveFileName(this, "Scene fingerprint");
    if(fileName.isEmpty())
    {
        return;
    }
    fileName += ".txt";
    QFile saveFile(fileName);
    if(!saveFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        return;
    }
    QTextStream saveStream(&saveFile);

    auto* const gl = context()->extraFunctions();

    gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_pData->propsSSBO);
    auto* rawMappedData = gl->glMapBufferRange(
        GL_SHADER_STORAGE_BUFFER,
        0,
        m_pData->propsMaxNum * sizeof(Prop),
        GL_MAP_READ_BIT
    );
    auto mappedData = static_cast<Prop*>(rawMappedData);

    for(int i = 0; i < m_pData->propsMaxNum; ++i)
    {
        serialize(mappedData[i], saveStream);
    }
    saveFile.close();

    gl->glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);
}

void OpenGLWidget::onSpeedChanged(int speed)
{
    m_pData->computeSpeed = speed;
}

void OpenGLWidget::initializeGL()
{
    QOpenGLExtraFunctions* const gl = context()->extraFunctions();

    m_pData->computeProgram = std::make_unique<QOpenGLShaderProgram>(this);
    loadShaderProgram(
        m_pData->computeProgram.get(),
        ":/shaders/compute.cmp"
    );
    m_pData->computeProgram->bind();
    m_pData->computeProgram->setUniformValue("numOfParticles", static_cast<GLint>(m_pData->props.size()));
    m_pData->computeProgram->release();

    m_pData->renderProgram = std::make_unique<QOpenGLShaderProgram>(this);
    loadShaderProgram(
        m_pData->renderProgram.get(),
        ":/shaders/render.vert",
        ":/shaders/render.frag"
    );

    gl->glGenBuffers(1, &m_pData->renderUBO);

    gl->glGenVertexArrays(1, &m_pData->renderVAO);
    gl->glGenBuffers(1, &m_pData->renderEBO);
    gl->glBindVertexArray(m_pData->renderVAO);

    QVector<GLuint> renderInstanceIndexes = { 0, 1, 3, 0, 3, 2 };
    gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pData->renderEBO);
    gl->glBufferData(
        GL_ELEMENT_ARRAY_BUFFER,
        (renderInstanceIndexes.size() * static_cast<int>(sizeof(unsigned int))),
        renderInstanceIndexes.data(),
        GL_STATIC_DRAW
    );
    gl->glBindVertexArray(0); // Unbind VAO

    gl->glGenBuffers(1, &m_pData->propsSSBO);
    gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, m_pData->propsSSBO);
    gl->glBufferData(
        GL_SHADER_STORAGE_BUFFER,
        m_pData->propsMaxNum * sizeof(Prop),
        m_pData->props.data(),
        GL_DYNAMIC_READ
    );
    gl->glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, m_pData->propsSSBO);
    gl->glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    m_pData->props.clear();
}

void OpenGLWidget::compute()
{
    QOpenGLExtraFunctions* const gl = context()->extraFunctions();

    m_pData->computeProgram->bind();
    for(int i = 0; i < m_pData->computeSpeed; ++i)
    {
        //int const numOfIterations = std::pow(m_pData->props.size(), 2) / TOTAL_THREADS_NUM;
        //for(int j = 0; j < numOfIterations + 1; ++j)
        //{
        //    m_pData->computeProgram->setUniformValue("iteration", static_cast<GLuint>(j));
        //    m_pData->computeProgram->setUniformValue("numOfIterations", static_cast<GLuint>(numOfIterations));
            gl->glDispatchCompute(GROUP_NUM, 1, 1);
            //gl->glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        //}
    }
    m_pData->computeProgram->release();
}

void OpenGLWidget::paintGL()
{
    QOpenGLExtraFunctions* const gl = context()->extraFunctions();

    compute();

    QColor const bg{ 0, 0, 0 };
    gl->glClearColor
    (
        static_cast<GLclampf>(bg.redF()),
        static_cast<GLclampf>(bg.greenF()),
        static_cast<GLclampf>(bg.blueF()),
        static_cast<GLclampf>(bg.alphaF())
    );
    gl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    gl->glDisable(GL_DEPTH_TEST);
    gl->glDepthMask(GL_TRUE);
    gl->glDisable(GL_BLEND);

    m_pData->renderProgram->bind();

    auto const uniformCameraBlock = m_pData->camera.toUniform();
    gl->glBindBuffer(GL_UNIFORM_BUFFER, m_pData->renderUBO);
    gl->glBufferData(GL_UNIFORM_BUFFER, sizeof(uniformCameraBlock), reinterpret_cast<void const*>(&uniformCameraBlock), GL_DYNAMIC_DRAW);
    gl->glBindBufferBase(GL_UNIFORM_BUFFER, 0, m_pData->renderUBO);
    gl->glBindBuffer(GL_UNIFORM_BUFFER, 0);

    gl->glBindVertexArray(m_pData->renderVAO);
    gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_pData->renderEBO);
    gl->glDrawElementsInstanced(
        GL_TRIANGLES,
        6,
        GL_UNSIGNED_INT, nullptr, m_pData->propsMaxNum
    );
    gl->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    gl->glBindVertexArray(0);

    m_pData->renderProgram->release();

    QTimer::singleShot(0, [this]()
    {
        update();
    });
}

void OpenGLWidget::resizeGL(int w, int h)
{
    m_pData->camera.aspectRatio = static_cast<float>(w) / static_cast<float>(h);
}

void OpenGLWidget::loadShaderProgram(
    QOpenGLShaderProgram* pProgram,
    const QString& vertexShader,
    const QString& fragmentShader
)
{
    QString vs, fs;
    {
        QFile file(vertexShader);
        [[maybe_unused]] bool const opened = file.open(QIODevice::ReadOnly);
        assert(opened);
        QByteArray const code = file.readAll();
        file.close();
        vs += code.toStdString().c_str();
    }
    {
        QFile file(fragmentShader);
        [[maybe_unused]] bool const opened = file.open(QIODevice::ReadOnly);
        assert(opened);
        QByteArray const code = file.readAll();
        file.close();
        fs += code.toStdString().c_str();
    }
    pProgram->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vs);
    pProgram->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fs);
    Q_ASSERT(pProgram->link());
}

void OpenGLWidget::loadShaderProgram(
    QOpenGLShaderProgram* pProgram,
    const QString& computeShader
)
{
    QString cs;
    {
        QFile file(computeShader);
        [[maybe_unused]] bool const opened = file.open(QIODevice::ReadOnly);
        assert(opened);
        QByteArray const code = file.readAll();
        file.close();
        cs += code.toStdString().c_str();
    }
    pProgram->addCacheableShaderFromSourceCode(QOpenGLShader::Compute, cs);
    Q_ASSERT(pProgram->link());
}


void OpenGLWidget::mousePressEvent(QMouseEvent* pEvent)
{
    if(Qt::LeftButton & pEvent->buttons() || Qt::RightButton & pEvent->buttons())
    {
        m_pData->lastMousePoint = pEvent->pos();
    }
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent* pEvent)
{
    if(!m_pData->lastMousePoint.has_value())
    {
        return;
    }

    if(Qt::LeftButton & pEvent->buttons())
    {
        m_pData->camera.phi += 0.005 * (pEvent->pos().x() - m_pData->lastMousePoint.value().x());
        m_pData->camera.theta += 0.005 * (pEvent->pos().y() - m_pData->lastMousePoint.value().y());
    }
    else if(Qt::RightButton & pEvent->buttons())
    {
        float const sensivity = 1e-3f * m_pData->camera.distance();
        float const dx = sensivity * static_cast<float>(pEvent->x() - m_pData->lastMousePoint.value().x());
        float const dy = -sensivity * static_cast<float>(pEvent->y() - m_pData->lastMousePoint.value().y());
        QMatrix3x3 const m = m_pData->camera.rotate();
        QVector3D const right = { m(0, 0), m(0, 1), m(0, 2) };
        QVector3D const up = { m(1, 0), m(1, 1), m(1, 2) };
        m_pData->camera.at -= (right * dx + up * dy) / m_pData->camera.scale;
    }

    m_pData->lastMousePoint = pEvent->pos();
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent* pEvent)
{
    m_pData->lastMousePoint = std::nullopt;
}

void OpenGLWidget::wheelEvent(QWheelEvent* pEvent)
{
    static float const wheelPosMin = -15.f;
    static float const wheelPosMax = 5.f;
    m_pData->camera.wheelPos += 0.005f * pEvent->delta();
    m_pData->camera.wheelPos = std::min(wheelPosMax, std::max(wheelPosMin, m_pData->camera.wheelPos));
}

void OpenGLWidget::loadCostMap()
{
    srand(GetCurrentTime());
    m_pData->propsMaxNum = 1024 * 256;
    for(int i = 0; i < m_pData->propsMaxNum; ++i)
    {
        m_pData->props.push_back(Prop{
            QVector3D { (rand() % 1000) / 500.f - 1.f, (rand() % 1000) / 500.f - 1.f, (rand() % 1000) / 500.f - 1.f },
            QVector3D { (rand() % 1000) / 500.f - 1.f, (rand() % 1000) / 500.f - 1.f, (rand() % 1000) / 500.f - 1.f }
        });
        m_pData->props.back().velocity /= 1000.;
    }
}

void OpenGLWidget::loadParameters()
{
    auto* gl = context()->extraFunctions();
    int maxX, maxY, maxZ, maxItemsPerGroup;
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &maxX);
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &maxY);
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &maxZ);
    gl->glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &maxItemsPerGroup);
    qDebug() << "Max Group Threads(X, Y, Z, Sum):" << maxX << maxY << maxZ << maxItemsPerGroup;
    emit maxGroupThreadsCalculated(maxX, maxY, maxZ, maxItemsPerGroup);
    int maxGroupX, maxGroupY, maxGroupZ, maxSharedSize;
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &maxGroupX);
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &maxGroupY);
    gl->glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &maxGroupZ);
    gl->glGetIntegerv(GL_MAX_COMPUTE_SHARED_MEMORY_SIZE, &maxSharedSize);
    qDebug() << "Max Threads(X, Y, Z, Sum):" << maxGroupX << maxGroupY << maxGroupZ << maxSharedSize;
    emit maxThreadsCalculated(maxGroupX, maxGroupY, maxGroupZ, maxSharedSize);
}

} // namespace scene_handler