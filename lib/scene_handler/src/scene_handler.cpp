#include <scene_handler.h>
#include <scene_wd.h>
#include <common_tool_panel.h>
#include <info_tool_panel.h>

#include <QPainter>

namespace scene_handler
{

struct SceneHandler::PrivateData
{
    OpenGLWidget* pSceneWd;
    tool_panels::CommonToolPanel* pCommonToolPanel;
    tool_panels::InfoToolPanel* pInfoToolPanel;
};

SceneHandler::SceneHandler(QObject* pParent)
    : QObject(pParent)
    , m_pData(std::make_unique<PrivateData>())
{
    initHandler();
}

void SceneHandler::initHandler()
{
    m_pData->pSceneWd = new OpenGLWidget();
    m_pData->pSceneWd->resize(1200, 800);

    m_pData->pCommonToolPanel = new tool_panels::CommonToolPanel(m_pData->pSceneWd);
    connect(m_pData->pCommonToolPanel, &tool_panels::CommonToolPanel::saveButtonClicked, m_pData->pSceneWd, &OpenGLWidget::onSaveButtonClicked);
    connect(m_pData->pCommonToolPanel, &tool_panels::CommonToolPanel::speedChanged, m_pData->pSceneWd, &OpenGLWidget::onSpeedChanged);
    m_pData->pSceneWd->addPanel(m_pData->pCommonToolPanel);

    m_pData->pInfoToolPanel = new tool_panels::InfoToolPanel(m_pData->pSceneWd);
    m_pData->pInfoToolPanel->initInfoLine("MaxGroupThreads:", QStringLiteral(""));
    auto xGroupThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    X");
    auto yGroupThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Y");
    auto zGroupThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Z");
    auto sumGroupThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Sum");
    connect(m_pData->pSceneWd, &OpenGLWidget::maxGroupThreadsCalculated, [this
        , xGroupThreadsNumId
        , yGroupThreadsNumId
        , zGroupThreadsNumId
        , sumGroupThreadsNumId
    ](int xNum, int yNum, int zNum, int sumNum)
    { 
        m_pData->pInfoToolPanel->onInfoChanged(xGroupThreadsNumId, QString::number(xNum));
        m_pData->pInfoToolPanel->onInfoChanged(yGroupThreadsNumId, QString::number(yNum));
        m_pData->pInfoToolPanel->onInfoChanged(zGroupThreadsNumId, QString::number(zNum));
        m_pData->pInfoToolPanel->onInfoChanged(sumGroupThreadsNumId, QString::number(sumNum));
    });
    m_pData->pInfoToolPanel->initInfoLine("MaxThreads:", QStringLiteral(""));
    auto xThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    X");
    auto yThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Y");
    auto zThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Z");
    auto sumThreadsNumId = m_pData->pInfoToolPanel->initInfoLine("    Sum");
    connect(m_pData->pSceneWd, &OpenGLWidget::maxThreadsCalculated, [this
        , xThreadsNumId
        , yThreadsNumId
        , zThreadsNumId
        , sumThreadsNumId
    ](int xNum, int yNum, int zNum, int sumNum)
    {
        m_pData->pInfoToolPanel->onInfoChanged(xThreadsNumId, QString::number(xNum));
        m_pData->pInfoToolPanel->onInfoChanged(yThreadsNumId, QString::number(yNum));
        m_pData->pInfoToolPanel->onInfoChanged(zThreadsNumId, QString::number(zNum));
        m_pData->pInfoToolPanel->onInfoChanged(sumThreadsNumId, QString::number(sumNum));
    });
    m_pData->pSceneWd->addPanel(m_pData->pInfoToolPanel);
}

SceneHandler::~SceneHandler() = default;

QWidget* SceneHandler::getSceneWd()
{
    return m_pData->pSceneWd;
}

} // namespace scene_handler