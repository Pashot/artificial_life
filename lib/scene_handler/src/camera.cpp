#include <camera.h>

#include <QDebug>

QVector3D Camera::direction() const
{
    return QQuaternion::rotationTo({ 0.f, 0.f, 1.f }, up).rotatedVector
    ({
        std::cos(theta) * std::cos(phi),
        std::cos(theta) * std::sin(phi),
        std::sin(theta)
    });
}

float Camera::distance() const
{
    return baseDistance * std::pow(wheelSensivity, wheelPos);
}

float Camera::nearPlane() const
{
    return wheelPos > 0.1f ? distance() - baseDistance : 0.025f * distance();
}

float Camera::farPlane() const
{
    float const maxL = std::fmax(scale[0], std::fmax(scale[1], scale[2]));
    return maxL * (distance() + 2.f * baseDistance);
}

QVector3D Camera::relPosition() const
{
    return distance() * direction();
}

QVector3D Camera::position() const
{
    return relPosition() + scale * at;
}

QMatrix4x4 Camera::projection() const
{
    QMatrix4x4 m;

    float const n = nearPlane();
    float const f = farPlane();
    float const c = -2.f * n * f / (f - n);
    float const az = (-f - n) / (f - n);
    float const ax = 1.f / std::tan(0.5f * fov) / aspectRatio;
    float const ay = ax * aspectRatio;
    m = QMatrix4x4
    {
        ax, 0.f, 0.f, 0.f,
        0.f, ay, 0.f, 0.f,
        0.f, 0.f, az, c,
        0.f, 0.f, -1.f, 0.f,
    };

    return m;
}

QMatrix4x4 Camera::lookat() const
{
    QMatrix4x4 m;
    // order: -at -> scale -> -pos -> rotate
    m.translate(-relPosition());
    m.scale(scale);
    m.translate(-at);
    return QMatrix4x4(rotate()) * m;
}

QMatrix3x3 Camera::rotate() const
{
    bool right = false;

    QMatrix3x3 m;
    QVector3D const z = direction();
    QVector3D const x = (right ^ (static_cast<int>(std::fmod(std::floor(theta / 3.1415926535f + 0.5f), 2.f)) != 0)
        ? QVector3D::crossProduct(up, z)
        : QVector3D::crossProduct(z, up)
        ).normalized();
    QVector3D const y = right
        ? QVector3D::crossProduct(z, x)
        : QVector3D::crossProduct(x, z);
    for(int i = 0; i < 3; i++)
    {
        m(0, i) = x[i];
        m(1, i) = y[i];
        m(2, i) = z[i];
    }
    return m;
}

UniformCamera Camera::toUniform() const
{
    UniformCamera uniformCamera;

    QMatrix4x4 const P = projection();
    QMatrix3x3 const R = rotate();
    QVector3D const pos = relPosition();

    for(int i = 0; i < 4; ++i)
        for(int j = 0; j < 4; ++j)
            uniformCamera.projection[i][j] = P(j, i);
    for(int i = 0; i < 3; ++i)
    {
        for(int j = 0; j < 3; ++j)
            uniformCamera.rotate[i][j] = R(j, i);
        uniformCamera.at[i] = at[i];
        uniformCamera.position[i] = pos[i];
        uniformCamera.scale[i] = scale[i];
    }

    return uniformCamera;
}