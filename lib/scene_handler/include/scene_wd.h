#pragma once

#include <QWidget>
#include <QRect>
#include <QOpenGLShaderProgram>
#include <QOpenGLWidget>

#include <memory>

class QMouseEvent;
class QWheelEvent;
class QPaintEvent;
class QResizeEvent;
class QPainter;

namespace tool_panels
{

class AbstractToolPanel;

} // namespace tool_panels

namespace scene_handler
{

class OpenGLWidget : public QOpenGLWidget
{
    Q_OBJECT
public:

    OpenGLWidget(QWidget* pParent = nullptr);
    ~OpenGLWidget();

    void addPanel(tool_panels::AbstractToolPanel*);

public slots:

    void onSaveButtonClicked();
    void onSpeedChanged(int speed);

signals:

    void maxGroupThreadsCalculated(int xNum, int yNum, int zNum, int sumNum);
    void maxThreadsCalculated(int xNum, int yNum, int zNum, int sumNum);

private:

    void compute();

protected:

    QSize sizeHint() const override;
    void resizeEvent(QResizeEvent*) override;
    void initializeGL();
    void resizeGL(int w, int h);
    void paintGL();

    void mousePressEvent(QMouseEvent*) override;
    void mouseMoveEvent(QMouseEvent*) override;
    void mouseReleaseEvent(QMouseEvent*) override;
    void wheelEvent(QWheelEvent*) override;

private:

    void loadShaderProgram(
        QOpenGLShaderProgram* pProgram,
        const QString& vertexShader,
        const QString& fragmentShader
    );
    void loadShaderProgram(
        QOpenGLShaderProgram* pProgram,
        const QString& computeShader
    );
    void loadCostMap();
    void loadParameters();

private:

    struct PrivateData;
    std::unique_ptr<PrivateData> m_pData;
};

} // namespace scene_handler