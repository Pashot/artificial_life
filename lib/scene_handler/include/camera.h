#pragma once

#include <QVector3D>
#include <QMatrix4x4>
#include <QMatrix3x3>

struct UniformCamera
{
    float projection[4][4];
    float rotate[3][4];
    float at[4];
    float position[4];
    float scale[4];
};

struct Camera
{
    float phi = 0.f;
    float theta = 0.f;

    float baseDistance = 1.f;
    float fov = 0.7854f;
    float aspectRatio = 1.f;

    float wheelPos = 0.f;
    float wheelSensivity = 1.4f;

    QVector3D at = { 0.f, 0.f, 0.f };
    QVector3D up = { 0.f, 0.f, -1.f };
    QVector3D scale = { 1.f, 1.f, 1.f };

    QMatrix4x4 projection() const;
    float distance() const;
    float nearPlane() const;
    float farPlane() const;

    QVector3D direction() const;
    QVector3D relPosition() const;
    QVector3D position() const;
    QMatrix4x4 lookat() const;
    QMatrix3x3 rotate() const;

    UniformCamera toUniform() const;
};