#pragma once

#include<QObject>

#include <memory>

class QWidget;

namespace scene_handler
{

class SceneHandler : public QObject
{
    Q_OBJECT
public:

    SceneHandler(QObject* pParent = nullptr);
    ~SceneHandler();

    QWidget* getSceneWd();

private:

    void initHandler();

private:

    struct PrivateData;
    std::unique_ptr<PrivateData> m_pData;
};

} // namespace scene_handler