#pragma once

#include <abstract_tool_panel.h>

namespace tool_panels
{

class CommonToolPanel : public AbstractToolPanel
{
    Q_OBJECT
public:

    CommonToolPanel(QWidget* pParent = nullptr);
    ~CommonToolPanel() override;

signals:

    void saveButtonClicked();
    void speedChanged(int);

private:

    void initToolPanel();

protected:

    void moveSelfImpl() override;
};

} // namespace tool_panels
