#pragma once

#include <QWidget>

namespace tool_panels
{

class AbstractToolPanel : public QWidget
{
    Q_OBJECT
public:

    AbstractToolPanel(QWidget* pParent = nullptr);
    ~AbstractToolPanel() override;

    void moveSelf();

protected:

    void showEvent(QShowEvent* pEvent) override;
    virtual void moveSelfImpl() = 0;
};

} // namespace tool_panels
