#pragma once

#include <abstract_tool_panel.h>

#include <QUuid>

#include <memory>

namespace tool_panels
{

class InfoToolPanel : public AbstractToolPanel
{
    Q_OBJECT
public:

    InfoToolPanel(QWidget* pParent = nullptr);
    ~InfoToolPanel() override;

    QUuid initInfoLine(QString const& lineHeader, QUuid const& lineId = QUuid::createUuid());
    void initInfoLine(QString const& lineHeader, QString const& lineInfo);

public slots:

    void onInfoChanged(QUuid const& lineId, QString const& lineInfo);

private:

    void initToolPanel();

protected:

    void moveSelfImpl() override;

private:

    struct PrivateData;
    std::unique_ptr<PrivateData> m_pData;
};

} // namespace tool_panels
