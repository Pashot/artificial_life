#include <abstract_tool_panel.h>

#include <QTimer>


namespace tool_panels
{

AbstractToolPanel::AbstractToolPanel(QWidget* pParent)
    : QWidget(pParent)
{
}

AbstractToolPanel::~AbstractToolPanel() = default;

void AbstractToolPanel::moveSelf()
{
    if (nullptr != parentWidget())
    {
        moveSelfImpl();
    }
}

void AbstractToolPanel::showEvent(QShowEvent* pEvent)
{
    QWidget::showEvent(pEvent);
    QTimer::singleShot(0, this, [this]()
    {
        adjustSize();
        moveSelf();
    });
}

} // namespace tool_panels
