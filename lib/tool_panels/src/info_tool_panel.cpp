#include <info_tool_panel.h>

#include <QLayout>
#include <QLabel>
#include <QHash>
#include <QDebug>

namespace tool_panels
{
namespace
{

int const MARGIN{ 16 };
QColor const FONT_COLOR = QColor(255, 255, 255);

} // namespace

struct InfoToolPanel::PrivateData
{
    QHash<QUuid, QLabel*> infoLabels;
    QGridLayout* pLayout{ nullptr };
};

InfoToolPanel::InfoToolPanel(QWidget* pParent)
    : AbstractToolPanel(pParent)
    , m_pData(std::make_unique<PrivateData>())
{
    initToolPanel();
    setStyleSheet("QWidget[primary = true] { border-radius: 4px; color: #202020; background-color: #a0ffffff }");
}

InfoToolPanel::~InfoToolPanel() = default;

void InfoToolPanel::initToolPanel()
{
    setLayout(new QVBoxLayout());
    layout()->setContentsMargins(0, 0, 0, 0);
    auto* pPrimaryWd = new QWidget(this);
    pPrimaryWd->setProperty("primary", "true");
    layout()->addWidget(pPrimaryWd);
    m_pData->pLayout = new QGridLayout(pPrimaryWd);
    pPrimaryWd->setLayout(m_pData->pLayout);
}

QUuid InfoToolPanel::initInfoLine(QString const& lineHeader, QUuid const& lineId)
{
    auto* pHeaderLabel = new QLabel(lineHeader, this);
    m_pData->pLayout->addWidget(pHeaderLabel, m_pData->pLayout->rowCount(), 0);
    auto* pInfoLabel = new QLabel(this);
    m_pData->pLayout->addWidget(pInfoLabel, m_pData->pLayout->rowCount() - 1, 1);
    m_pData->infoLabels.insert(lineId, pInfoLabel);

    return lineId;
}

void InfoToolPanel::initInfoLine(QString const& lineHeader, QString const& lineInfo)
{
    auto* pHeaderLabel = new QLabel(lineHeader, this);
    m_pData->pLayout->addWidget(pHeaderLabel, m_pData->pLayout->rowCount(), 0);
    auto* pInfoLabel = new QLabel(lineInfo, this);
    m_pData->pLayout->addWidget(pInfoLabel, m_pData->pLayout->rowCount() - 1, 1);
}

void InfoToolPanel::onInfoChanged(QUuid const& lineId, QString const& lineInfo)
{
    auto infoLabelIter = m_pData->infoLabels.find(lineId);
    if(m_pData->infoLabels.end() == infoLabelIter)
    {
        return;
    }
    if(nullptr == infoLabelIter.value())
    {
        return;
    }
    
    infoLabelIter.value()->setText(lineInfo);
}

void InfoToolPanel::moveSelfImpl()
{
    move(parentWidget()->width() - width() - MARGIN, MARGIN);
}

} // namespace tool_panels
