#include <common_tool_panel.h>

#include <QLayout>
#include <QToolButton>
#include <QSlider>
#include <QButtonGroup>
#include <QSvgRenderer>
#include <QPainter>
#include <QApplication>

namespace tool_panels
{
namespace
{

int const BOTTOM_MARGIN{ 16 };
QSize const ICON_SIZE{ 70, 70 };

} // namespace

CommonToolPanel::CommonToolPanel(QWidget* pParent)
    : AbstractToolPanel(pParent)
{
    initToolPanel();
}

CommonToolPanel::~CommonToolPanel() = default;

void CommonToolPanel::initToolPanel()
{
    setLayout(new QHBoxLayout());

    auto pSaveButton = new QToolButton(this);
    pSaveButton->setFixedSize(ICON_SIZE);
    pSaveButton->setText("Save");
    this->layout()->addWidget(pSaveButton);
    connect(pSaveButton, &QToolButton::clicked, this, &CommonToolPanel::saveButtonClicked);

    auto pSpeedSlider = new QSlider(this);
    pSpeedSlider->setRange(1, 100);
    pSpeedSlider->setToolTip("Speed");
    connect(pSpeedSlider, &QSlider::valueChanged, this, &CommonToolPanel::speedChanged);
    pSpeedSlider->setValue(1);
    this->layout()->addWidget(pSpeedSlider);
}

void CommonToolPanel::moveSelfImpl()
{
    move((parentWidget()->width() - width()) / 2, parentWidget()->height() - height() - BOTTOM_MARGIN);
}

} // namespace tool_panels
