#include <scene_handler.h>

#include <QApplication>
#include <QWidget>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{

    auto fmt = QSurfaceFormat::defaultFormat();
    fmt.setDepthBufferSize(4);
    fmt.setSamples(4);

    // opengl core 4.0 profile
    {
        fmt.setProfile(QSurfaceFormat::CoreProfile);
        fmt.setVersion(4, 3);
        QSurfaceFormat::setDefaultFormat(fmt);
        QGuiApplication::setAttribute(Qt::AA_ShareOpenGLContexts);
        QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
        QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    }

    QApplication application(argc, argv);

    scene_handler::SceneHandler sceneHandler;
    sceneHandler.getSceneWd()->showMaximized();

    return application.exec();
}
